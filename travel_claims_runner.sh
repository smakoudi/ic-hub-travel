pip3 install boto3
pip3 install psycopg2-binary
pip3 install --upgrade pandas
pip3 install numpy
spark-submit --jars spark-xml_2.12-0.10.0.jar src/main_travel_claims.py > travel_claims_logs.log