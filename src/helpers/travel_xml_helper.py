import boto3
import json
import re
import sys
import datetime as dt
import time
import pyspark.sql.functions as F
import numpy as np
import pandas as pd
import psycopg2
import psycopg2.extras as extras
from io import StringIO

# Config parameters
IC_HUB_BUCKET = "ic-hub-travel-dev"
CLAIMS_DIRECTORY = "claims"
s3 = boto3.resource("s3")
client = boto3.client('s3')
claims_bucket = s3.Bucket(IC_HUB_BUCKET)


class TravelHelper:
    @staticmethod
    def is_file(s3_obj):
        """
        method to filter directory objects from file objects in s3 by key
        :param s3_obj:
        :return:
        """
        dir_pattern = re.search(".*\/$", s3_obj)
        if not dir_pattern:
            return True
        else:
            return False

    @staticmethod
    def move_rejected_file(clmh, obj):
        """
        method to move a rejected file object to the corresponding directory
        :param clmh:
        :param obj:
        """
        file_name = obj.key.replace(f"{CLAIMS_DIRECTORY}/{clmh[0]}/", "")
        copied_obj = client.copy_object(
            Key=f"{CLAIMS_DIRECTORY}/{clmh[0]}/rejected/{file_name}",
            Bucket=IC_HUB_BUCKET,
            CopySource=f"{IC_HUB_BUCKET}/{obj.key}")
        deleted_obj = client.delete_object(Bucket=IC_HUB_BUCKET, Key=f"{obj.key}")
        if copied_obj.get("ResponseMetadata").get("HTTPStatusCode") == 200 and deleted_obj.get("ResponseMetadata").get("HTTPStatusCode") == 204:
            print(
                f"info: file {file_name} successfully moved to directory {CLAIMS_DIRECTORY}/{clmh[0]}/rejected/"
            )
        else:
            print(
                f"error: error while moving file {file_name} to directory {CLAIMS_DIRECTORY}/{clmh[0]}/rejected/"
            )

    @staticmethod
    def get_levels_schema(str_schema):
        """
        Method to get tle names of columns from XML files as well as their nested levels
        :param str_schema:
        :return: [
                    {'name': 'CLAIM_BENEFICIARY', 'level': 1},
                    {'name': 'element', 'level': 2},
                    {'name': 'Birth_Date', 'level': 3},
                    {'name': 'CLAIM_BENEFICIARY_CID', 'level': 3},
                    {'name': 'Country_Code', 'level': 4},
                     ...]
        """
        flat_schema = []
        list_schema = str_schema.split('\n')
        for col in list_schema:
            lev = col.count('|')
            col_name = re.findall("(?<=\-- )(.*?)(?=\:)", col)
            if col_name:
                flat_schema.append({'name': col_name[0], 'level': lev})
        return flat_schema

    @staticmethod
    def flatten_xml_schema(df):
        """
        Method to flatten the schema of a nested dataframe
        :param df:
        :return: [
                    'CLAIM_BENEFICIARY',
                    'CLAIM_BENEFICIARY.Birth_Date',
                    'CLAIM_BENEFICIARY.CLAIM_BENEFICIARY_CID',
                    'CLAIM_BENEFICIARY.CLAIM_BENEFICIARY_CID.Country_Code',
                     ...]
        """
        res = []
        flat_schema = TravelHelper.get_levels_schema(df._jdf.schema().treeString())
        for i in range(len(flat_schema)):
            if flat_schema[i].get('level') == 1:
                res.append(flat_schema[i].get('name'))
            elif flat_schema[i - 1].get('level') < flat_schema[i].get('level'):
                res.append(res[i - 1] + '.' + flat_schema[i].get('name'))
            elif flat_schema[i - 1].get('level') == flat_schema[i].get('level'):
                res.append(re.findall(".*\.", res[i - 1])[0] + flat_schema[i].get('name'))
            elif flat_schema[i - 1].get('level') > flat_schema[i].get('level'):
                drop_lev = flat_schema[i - 1].get('level') - flat_schema[i].get('level')
                root_lev = re.findall(".*\.", res[i - 1])[0]
                for l in range(drop_lev):
                    if re.findall(".*\.", root_lev[:-1]):
                        root_lev = re.findall(".*\.", root_lev[:-1])[0]
                res.append(root_lev + flat_schema[i].get('name'))
        res = [x.replace('.element', '') for x in res]
        return list(set(res))

    @staticmethod
    def create_tmp_table(df, mapping, df_columns, table_name):
        """
        Method to create the tmp_claim_core_table
        """
        print(f" =======>  start creating temporary table {table_name}")
        try:
            tmp_table = df.select(
                F.explode(
                    F.array([
                        F.struct(
                            # [F.col(x) for x in xml_df_columns ]
                            [F.col(x.get("full_path")).alias(x.get("alias")) for x in mapping if
                             x.get("full_path") in df_columns]
                        )
                    ])).alias('all_columns')).select('all_columns.*')
            print(
                f"info: successfully created {table_name} with {len(tmp_table.columns)} columns and {tmp_table.count()} lines")
            return tmp_table
        except Exception as e:
            print(f"error: unable to create {table_name} : {e}")
            return

    @staticmethod
    def flatten_list_columns(df, mapping_flat_cols, mapping_cols_to_flatten, table_name):
        """
        method to flatten columns with list values
        :param df:
        :param mapping_flat_cols: list of columns that don't need flattening
        :param mapping_cols_to_flatten: list of columns to flatten
        :param table_name:
        :return:
        """
        df_cols = df.columns
        mapping_flat_cols = [cl for cl in mapping_flat_cols if cl in df_cols]
        cols_to_flatten = [c for c in mapping_cols_to_flatten if c in df_cols]
        # check if the mapping_cols_to_flatten are all of type list
        cols_to_flatten = [col[0] for col in df.dtypes if col[0] in cols_to_flatten and 'array' in col[1]]

        if len(cols_to_flatten) > 0:
            print(f"info: {len(cols_to_flatten)} columns with lists value to flatten for table {table_name}")
            try:
                flat_df = df.withColumn("all_col", F.arrays_zip(*cols_to_flatten)) \
                    .withColumn("all_col", F.explode("all_col")) \
                    .select(*mapping_flat_cols, *[F.col(f"all_col.{x}") for x in cols_to_flatten]
                            )
                flat_df.cache()
                print(
                    f"info: successfully created flat dataframe for {table_name} with {len(flat_df.columns)} columns and {flat_df.count()} rows.")
                print(f"info: the flat dataframe {table_name} is cached in memory.")
                return flat_df
            except Exception as e:
                print(f"error: unable to flatten dataframe: {e}")
        else:
            print(
                f"error: unable to flatten dataframe : no columns to flatten. Check the mapping for mapping_cols_to_flatten")
            return df
        return

    @staticmethod
    def insert_in_redshift(df, redshift_table, url, driver, user, password):
        print(f"info: start inserting data in {redshift_table}")
        try:
            df_to_insert = df.na.fill("")
            df_to_insert.write.format('jdbc') \
                .options(url=url,
                         driver=driver,
                         dbtable=redshift_table,
                         user=user,
                         password=password) \
                .mode("append").save()
            print(f"info: successfully inserted {df_to_insert.count()} rows in {redshift_table}")
        except Exception as e:
            print(f"error: unable to insert data in {redshift_table}: {e}")

    @staticmethod
    def connect_redshift(params_dic):
        try:
            # connect to the PostgreSQL server
            print('Connecting to Redshift')
            conn = psycopg2.connect(**params_dic)
        except (Exception, psycopg2.DatabaseError) as error:
            print(f"error: unable to connect to Redshift: {error}")
            sys.exit(1)
        print("Connection successful")
        return conn

    @staticmethod
    def insert_into_redshift(conn, dataframe, table):
        """
        Using cursor.mogrify() to build the bulk insert query
        then cursor.execute() to execute the query
        """
        # Create a list of tupples from the dataframe values
        print(f"info: start inserting data in redshift for table {table}")
        dataframe = dataframe.select([F.col(c).cast("string") for c in dataframe.columns])
        df = dataframe.toPandas()
        df = df.replace({np.nan: None})
        tuples = [tuple(x) for x in df.to_numpy()]
        # Comma-separated dataframe columns
        cols = ','.join(list(df.columns))
        # SQL query to execute
        cursor = conn.cursor()
        values = [cursor.mogrify("(" + ("%s," * len(df.columns))[:-1] + ")", tup).decode('utf8') for tup in tuples]
        str_values = ",".join(values)
        query = "INSERT INTO %s (%s) VALUES " % (table, cols) + str_values
        try:
            cursor.execute(query)
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(f"error: unable to insert data in {table} : {error}")
            conn.rollback()
            cursor.close()
            return 1
        print(f"info: successfully inserted data in {table}")
        cursor.close()
