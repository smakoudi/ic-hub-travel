
class RejectLogs:

    def __init__(self, clmh_prefix_obj):
        self.clmh_prefix = clmh_prefix_obj

    def insert_reject_logs(self): #TODO: add condition to insert only today's logs
        insert_reject_logs = f"""
                 INSERT INTO ic_hub_logs.reject_record
                (id_reference_line, id_provider, provider_name, code_reference, code_product, error_msg, 
                file_name, fg_recycled, dt_recyclage, project, business_line, dt_insertion)
                SELECT 
                    CAST (c.claim_number as integer),
                    c.code_claimhandler,
                    'test_clmh',
                    c.claim_number,
                    c.code_product,
                    c.errormsg,
                    c.file_name,
                    c.fg_recycled,
                    c.dt_recyclage,
                    'CLAIMS',
                    'TRAVEL',
                    c.dt_insertion
                FROM travel.CLAIM_REJECT c  
        """
        return insert_reject_logs