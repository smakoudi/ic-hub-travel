from .travel_queries_helper import QueriesRejectRGF, QueriesRejectREF, DeleteTables, DeleteDuplicate, InsertHistTables, DeleteExistClaims,InsertFinalTables,QueriesFactClaims
from .redshift_manager import RedshiftManager
from .log_tables_helper import RejectLogs


class RejectTables:
    @staticmethod
    def insert_reject_rgf(conn, file_name):
        nb_incident_date = RedshiftManager.execute_sql_query(QueriesRejectRGF(file_name).incident_date(), conn, "INCIDENT_DATE")
        nb_report_date = RedshiftManager.execute_sql_query(QueriesRejectRGF(file_name).report_date(), conn, "REPORT_DATE")
        nb_extraction_date = RedshiftManager.execute_sql_query(QueriesRejectRGF(file_name).extraction_date(), conn, "EXTRACTION_DATE")
        nb_last_update_date = RedshiftManager.execute_sql_query(QueriesRejectRGF(file_name).last_update_date(), conn, "LAST_UPDATE_DATE")
        nb_close_date_3 = RedshiftManager.execute_sql_query(QueriesRejectRGF(file_name).close_date_3(), conn, "CLOSE_DATE_3")
        nb_lose_date_1 = RedshiftManager.execute_sql_query(QueriesRejectRGF(file_name).close_date_1(), conn, "CLOSE_DATE_1")
        nb_payment_date = RedshiftManager.execute_sql_query(QueriesRejectRGF(file_name).payment_date(), conn, "PAYMENT_DATE")
        nb_event_date = RedshiftManager.execute_sql_query(QueriesRejectRGF(file_name).event_date(), conn, "EVENT_DATE")
        nb_reserve_amount = RedshiftManager.execute_sql_query(QueriesRejectRGF(file_name).reserve_amount(), conn, "RESERVE_AMOUNT")
        nb_debit_credit = RedshiftManager.execute_sql_query(QueriesRejectRGF(file_name).debit_credit(), conn, "DEBIT_CREDIT")

        return nb_incident_date + nb_report_date + nb_extraction_date + nb_last_update_date + nb_close_date_3 + \
               nb_lose_date_1 + nb_payment_date + nb_event_date + nb_reserve_amount + nb_debit_credit

    @staticmethod
    def insert_reject_ref(conn, file_name):

        nb_product_code = RedshiftManager.execute_sql_query(QueriesRejectREF(file_name).product_code(), conn, "PRODUCT_CODE")
        nb_trt_cause_code = RedshiftManager.execute_sql_query(QueriesRejectREF(file_name).trt_cause_code(), conn, "TRT_CAUSE_CODE")
        nb_claim_close_status = RedshiftManager.execute_sql_query(QueriesRejectREF(file_name).claim_close_status(), conn, "CLAIM_CLOSE_STATUS")
        nb_code_occurence_type = RedshiftManager.execute_sql_query(QueriesRejectREF(file_name).code_occurence_type(), conn, "CODE_OCCURENCE_TYPE")
        nb_code_service_type = RedshiftManager.execute_sql_query(QueriesRejectREF(file_name).code_service_type(), conn, "CODE_SERVICE_TYPE")
        nb_coverage_identifier = RedshiftManager.execute_sql_query(QueriesRejectREF(file_name).coverage_identifier(), conn, "COVERAGE_IDENTIFIER")
        nb_supplier_country_code = RedshiftManager.execute_sql_query(QueriesRejectREF(file_name).service_supplier_country_code(), conn, "SERVICE_SUPPLIER_COUNTRY_CODE")
        nb_beneficiary_country_code = RedshiftManager.execute_sql_query(QueriesRejectREF(file_name).claimt_beneficiary_country_code(), conn, "CLAIMT_BENEFICIARY_COUNTRY_CODE")

        return nb_product_code + nb_trt_cause_code + nb_claim_close_status + nb_code_occurence_type + nb_code_service_type + \
               nb_coverage_identifier + nb_supplier_country_code + nb_beneficiary_country_code

    @staticmethod
    def insert_reject_logs(conn, clmh_prefix):
        nb_reject_logs = RedshiftManager.execute_sql_query(RejectLogs(clmh_prefix).insert_reject_logs(), conn, "REJECT_LOGS")
        return nb_reject_logs

    @staticmethod
    def delete_staging_tables(conn):
        nb_deleted_rows = RedshiftManager.execute_sql_query(DeleteTables.DELETE_STAGING_TABLES, conn, "DELETE_STAGING_TABLES")
        return nb_deleted_rows

    @staticmethod
    def delete_duplicates(conn):
        nb_duplicate_rows = RedshiftManager.execute_sql_query(DeleteDuplicate.DELETE_DUPLICATE_STAGING_TABLES, conn, "DELETE_DUPLICATE_STAGING_TABLES")
        return nb_duplicate_rows


class FinalTables:
    @staticmethod
    def insert_fianl_tables(conn):
        nb_final_rows = RedshiftManager.execute_sql_query(InsertFinalTables.INSERT_FINAL_TABLES, conn, "INSERT_FINAL_TABLES")
        return nb_final_rows

    @staticmethod
    def insert_hist_tables(conn):
        nb_hist_rows = RedshiftManager.execute_sql_query(InsertHistTables.INSERT_HIST_TABLES, conn, "INSERT_HIST_TABLES")
        return nb_hist_rows

    @staticmethod
    def delete_exist_claims(conn):
        nb_existing_rows = RedshiftManager.execute_sql_query(DeleteExistClaims.DELETE_EXISTING_CLAIMS, conn, "DELETE_EXISTING_CLAIMS")
        return nb_existing_rows

    @staticmethod
    def alim_fact_claims(conn,period):
        alim_fact_claims = RedshiftManager.execute_sql_query(QueriesFactClaims(period).alim_fact_claims(), conn, "FACT_CLAIMS")
        return alim_fact_claims
