
class QueriesRejectREF(object):

    def __init__(self, file_name_obj):
        self.file_name = file_name_obj

    def product_code(self):
        product_code = f"""
                INSERT INTO travel.CLAIM_REJECT 
                (Routing_Item, Source_program, Code_ClaimHandler, Claim_Number, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage) 
                SELECT
                      c.Routing_Item
                      ,c.Source_program
                      ,c.Code_ClaimHandler
                      ,c.Claim_Number
                      ,c.Dt_Insertion
                      ,true as Fg_Rejected
                      ,'Invalid PPUC code (Policy Agreement Field)  ['+c.Code_product +']' as ErrorMsg
                      , '{self.file_name}'
                      ,c.Code_product
                      ,false as fg_recycled
                      ,null as dt_recyclage 
                  FROM travel.TMP_CLAIM_CORE c
                  LEFT OUTER JOIN travel.REF_PRODUCT lp ON c.Code_Product = lp.Code_Product /*ou EAIB_PRODUCT_CODE*/
                  WHERE lp.Code_Product IS NULL ;
        """
        return product_code

    def trt_cause_code(self):
        trt_cause_code = f"""
            INSERT INTO travel.CLAIM_REJECT 
            (Routing_Item, Source_program, Code_ClaimHandler, Claim_Number, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage) 
            SELECT
                  c.Routing_Item
                  ,c.Source_program
                  ,c.Code_ClaimHandler
                  ,c.Claim_Number
                  ,c.Dt_Insertion
                  ,true as Fg_Rejected
                  ,'Invalid Claim Close Code ['+c.Code_Trt_Cause +']' as ErrorMsg
                  , '{self.file_name}'
                  ,c.Code_product
                  ,false as fg_recycled
                  ,null as dt_recyclage 
              FROM travel.TMP_CLAIM_core c
              LEFT OUTER JOIN travel.ref_list cc ON c.Code_Trt_Cause = cc.code_ref
              WHERE cc.type_ref='CLAIMS_CAUSES' and  cc.code_ref IS NULL  and c.Code_Trt_Cause is not null ;
        """
        return trt_cause_code

    def claim_close_status(self):
        claim_close_status = f"""
            INSERT INTO travel.CLAIM_REJECT 
            (Routing_Item, Source_program, Code_ClaimHandler, Claim_Number, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage) 
            SELECT
                  c.Routing_Item
                  ,c.Source_program
                  ,c.Code_ClaimHandler
                  ,c.Claim_Number
                  ,c.Dt_Insertion
                  ,true as Fg_Rejected
                  ,'Invalid Claim Close (Sub) Status ['+cast(c.code_claim_close_status as varchar)+']' as ErrorMsg
                  , '{self.file_name}'   
                  ,c.Code_product      
                  ,false as fg_recycled      
                  ,null as dt_recyclage 
              FROM travel.TMP_CLAIM_CORE c
              LEFT OUTER JOIN travel.ref_list cs ON c.code_claim_close_status = cs.code_ref
              WHERE cs.type_ref="code_claim_close_status" and cs.code_ref IS NULL and c.code_claim_close_status is not null ;
        """
        return claim_close_status

    def code_occurence_type(self):
        code_occurence_type = f"""
            INSERT INTO travel.CLAIM_REJECT 
            (Routing_Item, Source_program, Code_ClaimHandler, Claim_Number, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage)   
              SELECT
                  c.Routing_Item
                  ,c.Source_program
                  ,c.Code_ClaimHandler
                  ,c.Claim_Number
                  ,c.Dt_Insertion
                  ,true as Fg_Rejected
                  ,  'Invalid Occurrence type identifier ['+cast(c.Code_Occurrence_Type as varchar)+']' as ErrorMsg
                  , '{self.file_name}'  
                  ,c.Code_product      
                  ,false as fg_recycled      
                  ,null as dt_recyclage 
              FROM travel.TMP_CLAIM_CORE c
              LEFT OUTER JOIN travel.ref_list oi ON c.Code_Occurrence_Type = oi.code_ref
              WHERE oi.type_ref='OCCURRENCE_TYPE' and  oi.code_ref IS NULL ;
        """
        return code_occurence_type

    def code_service_type(self):
        code_service_type = f"""
            INSERT INTO travel.CLAIM_REJECT 
            (Routing_Item, Source_program,Code_ClaimHandler, Claim_Number,  Id_Service_Order, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage)  
              SELECT
                    s.Routing_Item
                   , s.Source_program
                    ,c.Code_ClaimHandler
                   , s.Claim_Number
                   , Id_Service_Order
                    ,s.Dt_Insertion
                   ,true as Fg_Rejected
               ,'Invalid Service type code ['+cast(s.Code_Service_Type as varchar)+']' as ErrorMsg
               , '{self.file_name}'   
               ,c.Code_product      
               ,false as fg_recycled      
               ,null as dt_recyclage 
             FROM travel.TMP_CLAIM_SERVICE s
             LEFT OUTER JOIN travel.ref_list  tc ON s.Code_Service_Type = tc.code_ref
             LEFT OUTER JOIN travel.TMP_CLAIM_CORE c on s.Claim_Number = c.Claim_Number
                                            and s.Routing_Item = c.Routing_Item
                                            and s.Source_program = c.Source_program
             where tc.type_ref='SERVICE_TYPE' and tc.code_ref is NULL and s.Code_Service_Type  is not null ;
        """
        return code_service_type

    def coverage_identifier(self):
        coverage_identifier = f"""
            INSERT INTO travel.CLAIM_REJECT 
            (Routing_Item, Source_program, Code_ClaimHandler, Claim_Number, Id_Service_Order, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage)    
                SELECT 
                    s.Routing_Item
                   , s.Source_program
                    ,c.Code_ClaimHandler
                   , s.Claim_Number
                   , Id_Service_Order
                  ,s.Dt_Insertion
                  ,true as Fg_Rejected
               ,'Invalid Service Coverage Identifier ['+cast(s.code_coverage as varchar)+']' as ErrorMsg
               , '{self.file_name}'   
               ,c.Code_product      
               ,false as fg_recycled      
               ,null as dt_recyclage 
             FROM travel.TMP_CLAIM_SERVICE s
             LEFT OUTER JOIN travel.ref_list ci ON s.code_coverage = ci.code_ref
              LEFT OUTER JOIN travel.TMP_CLAIM_CORE c on s.Claim_Number = c.Claim_Number
                                            and s.Routing_Item = c.Routing_Item
                                            and s.Source_program = c.Source_program
             where ci.type_ref='COVERAGE' and ci.code_ref is NULL  and s.code_coverage is not null;
        """
        return coverage_identifier

    def service_supplier_country_code(self):
        service_supplier_country_code = f"""
            INSERT INTO travel.CLAIM_REJECT 
            (Routing_Item, Source_program , Code_ClaimHandler, Claim_Number, Id_Service_Order, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage)  
              SELECT 
                    s.Routing_Item
                   , s.Source_program
                   ,c.Code_ClaimHandler
                   , s.Claim_Number
                   , Id_Service_Order
                    ,s.Dt_Insertion
                   ,true as Fg_Rejected
               ,'Invalid Service Country Code ['+s.Code_Country+']' as ErrorMsg
               , '{self.file_name}'    
               ,c.Code_product      
               ,false as fg_recycled      
               ,null as dt_recyclage 
             FROM travel.TMP_CLAIM_SERVICE s
             LEFT OUTER JOIN travel.ref_list cy ON s.Code_Country = cy.code_ref
             LEFT OUTER JOIN travel.TMP_CLAIM_CORE c on s.Claim_Number = c.Claim_Number
                                            and s.Routing_Item = c.Routing_Item
                                            and s.Source_program = c.Source_program
             where cy.type_ref='CODE_COUNTRY' and cy.code_ref IS NULL and s.Code_Country is not null ;
        """
        return service_supplier_country_code

    def claimt_beneficiary_country_code(self):
        claimt_beneficiary_country_code = f"""
            INSERT INTO travel.CLAIM_REJECT 
            (Routing_Item, Source_program, Code_ClaimHandler, Claim_Number, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage)    
              SELECT
               cp.Routing_Item
               , cp.Source_program
                ,c.Code_ClaimHandler
               , cp.Claim_Number
               , cp.Dt_Insertion
               ,true as Fg_Rejected
               ,CASE WHEN cp.party_type = 'Claimant' THEN 'Invalid Claimant Country Code ['+cp.Code_Country+']'
                    WHEN cp.party_type = 'Beneficiary' THEN 'Invalid Beneficiary Country Code ['+cp.Code_Country+']' 
                ELSE 'Invalid Country Code ['+cp.Code_Country+']'
                END as ErrorMsg
                , '{self.file_name}'    
                ,c.Code_product      
                ,false as fg_recycled      
                ,null as dt_recyclage 
              FROM travel.TMP_CLAIM_PARTY cp
              LEFT OUTER JOIN travel.ref_list cy ON cp.Code_Country = cy.code_ref
              LEFT OUTER JOIN travel.TMP_CLAIM_CORE c on cp.Claim_Number = c.Claim_Number
                                            and cp.Routing_Item = c.Routing_Item
                                            and cp.Source_program = c.Source_program
             where cy.type_ref ='CODE_COUNTRY' and cy.code_ref IS NULL 
              and cp.Code_Country is not null ; 
        """
        return claimt_beneficiary_country_code


class QueriesRejectRGF(object):

    def __init__(self, file_name_obj):
        self.file_name = file_name_obj

    def incident_date(self):
        incident_date = f"""
            INSERT INTO travel.CLAIM_REJECT 
            (Routing_Item, Source_program, Code_ClaimHandler, Claim_Number, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage)  
            SELECT
                  c.Routing_Item
                  ,c.Source_program
                  ,c.Code_ClaimHandler
                  ,c.Claim_Number
                  ,c.Dt_Insertion
                  ,true as Fg_Rejected
                  ,'The incident date must be less than the opening date ; ' as ErrorMsg
                  , '{self.file_name}'
                  ,c.Code_product
                  ,false as fg_recycled
                  ,null as dt_recyclage 
              FROM travel.TMP_CLAIM_CORE c
              WHERE DATEDIFF('second',dt_claim_open, Dt_Occurrence_Begin)>0;
        """
        return incident_date

    def report_date(self):
        report_date = f"""
            INSERT INTO travel.CLAIM_REJECT
            (Routing_Item, Source_program, Code_ClaimHandler, Claim_Number, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage)
            SELECT
                  c.Routing_Item
                  ,c.Source_program
                  ,c.Code_ClaimHandler
                  ,c.Claim_Number
                  ,c.Dt_Insertion
                  ,true as Fg_Rejected
                  ,'The claim reported_Date must be less than the closing date ; ' as ErrorMsg
                  , '{self.file_name}'
                  ,c.Code_product
                  ,false as fg_recycled
                  ,null as dt_recyclage
              FROM travel.TMP_CLAIM_CORE c
              WHERE DATEDIFF('second',dt_claim_close , dt_claim_reported )>0;
        """
        return report_date

    def extraction_date(self):
        extraction_date = f"""
            INSERT INTO travel.CLAIM_REJECT
            (Routing_Item, Source_program, Code_ClaimHandler, Claim_Number, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage)
              SELECT
                  c.Routing_Item
                  ,c.Source_program
                  ,c.Code_ClaimHandler
                  ,c.Claim_Number
                  ,c.Dt_Insertion
                  ,true as Fg_Rejected
                  ,'The claim Opening date must be less than the extraction date ; ' as ErrorMsg
                  , '{self.file_name}'
                  ,c.Code_product
                  ,false as fg_recycled
                  ,null as dt_recyclage
              FROM travel.TMP_CLAIM_CORE c
              left join travel.CLAIM_HEADER h on c.Routing_Item = h.Routing_Item and c.Source_Program = h.Source_Program
              WHERE DATEDIFF('day',h.dt_functional_extraction , dt_claim_open )>0;
        """
        return extraction_date

    def last_update_date(self):
        last_update_date = f"""
        INSERT INTO travel.CLAIM_REJECT
        (Routing_Item, Source_program, Code_ClaimHandler, Claim_Number, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage)
          SELECT
              c.Routing_Item
              ,c.Source_program
              ,c.Code_ClaimHandler
              ,c.Claim_Number
              ,c.Dt_Insertion
              ,true as Fg_Rejected
              ,'The claim last update date must be less than the extraction date ; ' as ErrorMsg
              , '{self.file_name}'
              ,c.Code_product
              ,false as fg_recycled
              ,null as dt_recyclage
          FROM travel.TMP_CLAIM_CORE c
          left join travel.CLAIM_HEADER h on c.Routing_Item = h.Routing_Item and c.Source_Program = h.Source_Program
          WHERE DATEDIFF('day',h.dt_functional_extraction , dt_claim_last_update )>0;
        """
        return last_update_date

    def close_date_3(self):
        close_date_3 = f"""
            INSERT INTO travel.CLAIM_REJECT
            (Routing_Item, Source_program, Code_ClaimHandler, Claim_Number, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage)
            SELECT
                  c.Routing_Item
                  ,c.Source_program
                  ,c.Code_ClaimHandler
                  ,c.Claim_Number
                  ,c.Dt_Insertion
                  ,true as Fg_Rejected
                  ,'Claim closing date is required for a closed claim ; ' as ErrorMsg
                  , '{self.file_name}'
                  ,c.Code_product
                  ,false as fg_recycled
                  ,null as dt_recyclage
              FROM travel.TMP_CLAIM_CORE c
              WHERE code_claim_status =3 and dt_claim_close  is null;
        """
        return close_date_3

    def close_date_1(self):
        close_date_1 = f"""
            INSERT INTO travel.CLAIM_REJECT
            (Routing_Item, Source_program, Code_ClaimHandler, Claim_Number, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage)
            SELECT
                  c.Routing_Item
                  ,c.Source_program
                  ,c.Code_ClaimHandler
                  ,c.Claim_Number
                  ,c.Dt_Insertion
                  ,true as Fg_Rejected
                  ,'A opening claim shouldn''t have a closing date ; ' as ErrorMsg
                  , '{self.file_name}'
                  ,c.Code_product
                  ,false as fg_recycled
                  ,null as dt_recyclage
              FROM travel.TMP_CLAIM_CORE c
              WHERE code_claim_status =1 and dt_claim_close  is not null;
        """
        return close_date_1

    def payment_date(self):
        payment_date = f"""
            INSERT INTO travel.CLAIM_REJECT
            (Routing_Item, Source_program, Code_ClaimHandler, Claim_Number, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage)
             SELECT
                  c.Routing_Item
                  ,c.Source_program
                  ,c.Code_ClaimHandler
                  ,c.Claim_Number
                  ,c.Dt_Insertion
                  ,true as Fg_Rejected
                  ,'The paiement date must be greater than the opening date ; ' as ErrorMsg
                  , '{self.file_name}'
                  ,c.Code_product
                  ,false as fg_recycled
                  ,null as dt_recyclage
            FROM travel.TMP_CLAIM_AMOUNT a
            LEFT OUTER JOIN travel.TMP_CLAIM_CORE c on c.Routing_Item = a.Routing_Item and c.Source_Program = a.Source_Program and c.Claim_Number = a.Claim_Number
            where a.code_amount_type  in (10003,10004)
            and DATEDIFF('day',a.dt_event , c.dt_claim_open )>0;
        """
        return payment_date

    def event_date(self):
        event_date = f"""
            INSERT INTO travel.CLAIM_REJECT
            (Routing_Item, Source_program, Code_ClaimHandler, Claim_Number, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage)
            SELECT
                  c.Routing_Item
                  ,c.Source_program
                  ,c.Code_ClaimHandler
                  ,c.Claim_Number
                  ,c.Dt_Insertion
                  ,true as Fg_Rejected
                  ,'Event date must be less than the closing date ; ' as ErrorMsg
                  , '{self.file_name}'
                  ,c.Code_product
                  ,false as fg_recycled
                  ,null as dt_recyclage
            FROM travel.TMP_CLAIM_AMOUNT a
            LEFT OUTER JOIN travel.TMP_CLAIM_CORE c on c.Routing_Item = a.Routing_Item and c.Source_Program = a.Source_Program and c.Claim_Number = a.Claim_Number
            where a.code_amount_type  in (10003,10004) and a.amount_level='S'
            and DATEDIFF('day',c.dt_claim_close , a.dt_event )>0;
        """
        return event_date

    def reserve_amount(self):
        reserve_amount = f"""
            INSERT INTO travel.CLAIM_REJECT
            (Routing_Item, Source_program, Code_ClaimHandler, Claim_Number, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage)
            SELECT
                  c.Routing_Item
                  ,c.Source_program
                  ,c.Code_ClaimHandler
                  ,c.Claim_Number
                  ,c.Dt_Insertion
                  ,true as Fg_Rejected
                  ,'A closed claim mustn''t have a reserve amount ; ' as ErrorMsg
                  , '{self.file_name}'
                  ,c.Code_product
                  ,false as fg_recycled
                  ,null as dt_recyclage
            FROM travel.TMP_CLAIM_AMOUNT a
            LEFT OUTER JOIN travel.TMP_CLAIM_CORE c on c.Routing_Item = a.Routing_Item and c.Source_Program = a.Source_Program and c.Claim_Number = a.Claim_Number
            where c.code_claim_status =3 and a.code_amount_type  in (10001,10002)
            and (a.claim_amount_excl_tax >0 or a.claim_amount_incl_tax>0)
            and a.amount_level !='F';
        """
        return reserve_amount

    def debit_credit(self):
        debit_credit = f"""
            INSERT INTO travel.CLAIM_REJECT
            (Routing_Item, Source_program, Code_ClaimHandler, Claim_Number, Dt_Insertion, Fg_Rejected, ErrorMsg, File_Name,Code_product,fg_recycled,dt_recyclage)
            SELECT
                  c.Routing_Item
                  ,c.Source_program
                  ,c.Code_ClaimHandler
                  ,c.Claim_Number
                  ,c.Dt_Insertion
                  ,true as Fg_Rejected
                  ,'Claims can only have Debit Type. Please correct the line and send it back with Type = D ; ' as ErrorMsg
                  , '{self.file_name}'
                  ,c.Code_product
                  ,false as fg_recycled
                  ,null as dt_recyclage
            FROM travel.TMP_CLAIM_AMOUNT a
            LEFT OUTER JOIN travel.TMP_CLAIM_CORE c on c.Routing_Item = a.Routing_Item and c.Source_Program = a.Source_Program and c.Claim_Number = a.Claim_Number
            where a.debit_credit !='D';
        """
        return debit_credit


class DeleteTables:
    DELETE_STAGING_TABLES = """
        delete FROM travel.TMP_CLAIM_CORE ;
        delete FROM travel.TMP_CLAIM_SERVICE ;
        delete FROM travel.TMP_CLAIM_AMOUNT;
        delete FROM travel.CLAIM_REJECT;
        delete FROM travel.TMP_CLAIM_PARTY;
    
    """


class DeleteDuplicate:
    DELETE_DUPLICATE_STAGING_TABLES = """
    
        /*Suppression des doublons de Claims*/
        delete FROM travel.TMP_CLAIM_CORE WHERE Id_Tmp_Claim in (
        
        SELECT Id_Tmp_Claim FROM  (
        SELECT
         Id_Tmp_Claim, Claim_Number, Dt_Claim_Last_Update, row_number() over (partition by Claim_Number order by Dt_Claim_Last_Update desc ) as rn
        from travel.TMP_CLAIM_CORE )q
        WHERE rn > 1
        
        ) ;
        
        /*Suppression des doublons de services*/
        DELETE FROM travel.TMP_CLAIM_SERVICE WHERE Id_Tmp_Service in (
        SELECT Id_Tmp_Service FROM  (
        
        SELECT
         Id_Tmp_Service, Claim_Number, Id_Service_Order, Dt_Claim_Last_Update, 
         row_number() over (partition by Claim_Number,Id_Service_Order order by Dt_Claim_Last_Update desc ) as rn
        from travel.TMP_CLAIM_SERVICE )q
        WHERE rn > 1
        
        ) ;
        
        /*Suppression des doublons de party claimant et benef*/
        
        DELETE FROM travel.TMP_CLAIM_PARTY WHERE Id_Tmp_Claim_Party in (
        SELECT Id_Tmp_Claim_Party FROM  (
        
        SELECT Id_Tmp_Claim_Party , Claim_Number, Dt_Claim_Last_Update, 
         row_number() over (partition by Claim_Number, Last_Name, First_Name,Party_Type  order by Dt_Claim_Last_Update desc ) as rn
        from travel.TMP_CLAIM_PARTY WHERE Party_Type <> 'Occurrence' )q
        WHERE rn > 1
        
        ) ;
        
        
        
        /*Suppression des doublons de service amounts*/
        DELETE FROM travel.TMP_CLAIM_AMOUNT WHERE Id_Tmp_Claim_Amount in (
        SELECT Id_Tmp_Claim_Amount FROM  (
        
        SELECT Id_Tmp_Claim_Amount , Claim_Number, Dt_Claim_Last_Update, 
         row_number() over (partition by Claim_Number, Id_Service_Order, Id_Claim_Amount_Order, Amount_Level,
         claim_amount_external_ref ,service_identifier_external_ref,claim_amount_incl_tax  order by Dt_Claim_Last_Update desc ) as rn
        from travel.TMP_CLAIM_AMOUNT WHERE Amount_Level = 'S' )q
        WHERE rn > 1
        
        ) ;
        
        
        /*Suppression des doublons de claims amounts*/
        
        DELETE FROM travel.TMP_CLAIM_AMOUNT WHERE Id_Tmp_Claim_Amount in (
        SELECT Id_Tmp_Claim_Amount FROM  (
        
        SELECT Id_Tmp_Claim_Amount , Claim_Number, Dt_Claim_Last_Update,  
         row_number() over (partition by Claim_Number, Amount_Level order by Dt_Claim_Last_Update desc ) as rn
        from travel.TMP_CLAIM_AMOUNT WHERE Amount_Level = 'C' )q
        WHERE rn > 1
        
        ) ;
        
        
        /*Suppression des doublons de first reserve amounts*/
        
        DELETE FROM travel.TMP_CLAIM_AMOUNT WHERE Id_Tmp_Claim_Amount in (
        SELECT Id_Tmp_Claim_Amount FROM  (
        
        SELECT Id_Tmp_Claim_Amount , Claim_Number, Dt_Claim_Last_Update,  
         row_number() over (partition by Claim_Number, Amount_Level order by Dt_Claim_Last_Update desc ) as rn
        from travel.TMP_CLAIM_AMOUNT WHERE Amount_Level = 'F' )q
        WHERE rn > 1
        
        ) ;
    
    """


class InsertHistTables:
    INSERT_HIST_TABLES = """
     /*Historisationdes Claim Amounts */ 
 
    INSERT INTO travel.HIST_CLAIM_AMOUNT
    SELECT  *,getdate() as Dt_Loading 
    FROM travel.CLAIM_AMOUNT
    WHERE id_claim  IN (
                        SELECT  c.Id_Claim from travel.TMP_CLAIM_CORE tmp
                        LEFT OUTER JOIN travel.CLAIM_CORE c ON tmp.Claim_Number = c.Claim_Number
                        WHERE c.Claim_Number IS NOT NULL 
                        AND tmp.Claim_Number NOT in (select Claim_Number FROM travel.CLAIM_REJECT)
                    )            
    ; 
    
    /*Historisation des claim Coverage service */ 
    INSERT INTO travel.HIST_CLAIM_COVERAGE_SERVICE 
    SELECT  *, getdate() as Dt_Loading
    FROM travel.CLAIM_COVERAGE_SERVICE WHERE Id_Claim in 
                        (
                            SELECT  c.Id_Claim from travel.TMP_CLAIM_CORE tmp
                            LEFT OUTER JOIN travel.CLAIM_CORE c ON tmp.Claim_Number = c.Claim_Number
                            WHERE c.Claim_Number IS NOT NULL 
                            AND tmp.Claim_Number NOT in (select Claim_Number FROM travel.CLAIM_REJECT)
                        ) ;
    
    
    
    
    /*Historisation des claim party */ 
    INSERT INTO travel.HIST_CLAIM_PARTY
    SELECT * , getdate() as Dt_Loading 
    FROM travel.CLAIM_PARTY WHERE Id_Claim in 
                        (
                            SELECT  c.Id_Claim from travel.TMP_CLAIM_CORE tmp
                            LEFT OUTER JOIN travel.CLAIM_CORE c ON tmp.Claim_Number = c.Claim_Number
                            WHERE c.Claim_Number IS NOT NULL 
                            AND tmp.Claim_Number NOT in (select Claim_Number FROM travel.CLAIM_REJECT)
                        ) ;  
    
    
    /*Historisation des claims existants */
    
    INSERT INTO travel.HIST_CLAIM_CORE
    SELECT *, getdate() as Dt_Loading
    FROM travel.CLAIM_CORE WHERE Id_Claim in 
                        (
                        SELECT  c.Id_Claim from travel.TMP_CLAIM_CORE tmp
                        LEFT OUTER JOIN travel.CLAIM_CORE c ON tmp.Claim_Number = c.Claim_Number
                        WHERE c.Claim_Number IS NOT NULL 
                        AND tmp.Claim_Number NOT in (select Claim_Number FROM travel.CLAIM_REJECT)
                        ) ;
    /*Historisation des rejets */                    
    INSERT INTO travel.hist_claim_reject
    SELECT *, getdate() as Dt_Loading
    FROM travel.claim_reject ;
    """


class DeleteExistClaims:
    DELETE_EXISTING_CLAIMS = """
    DELETE FROM travel.CLAIM_AMOUNT
    WHERE id_claim  IN (
                        SELECT  c.Id_Claim from travel.TMP_CLAIM_CORE tmp
                        LEFT OUTER JOIN travel.CLAIM_CORE c ON tmp.Claim_Number = c.Claim_Number
                        WHERE c.Claim_Number IS NOT NULL 
                        AND tmp.Claim_Number NOT in (select Claim_Number FROM travel.CLAIM_REJECT)
                    )  
    
    ; 
    
    /*Suppression des claim Coverage service */ 
    
    DELETE FROM travel.CLAIM_COVERAGE_SERVICE WHERE Id_Claim in 
                        (
                            SELECT  c.Id_Claim from travel.TMP_CLAIM_CORE tmp
                            LEFT OUTER JOIN travel.CLAIM_CORE c ON tmp.Claim_Number = c.Claim_Number
                            WHERE c.Claim_Number IS NOT NULL 
                            AND tmp.Claim_Number NOT in (select Claim_Number FROM travel.CLAIM_REJECT)
                        ) ; 
    
    
    /*Suppression des claim party */ 
    
    DELETE FROM travel.CLAIM_PARTY WHERE Id_Claim in 
                        (
                            SELECT  c.Id_Claim from travel.TMP_CLAIM_CORE tmp
                            LEFT OUTER JOIN travel.CLAIM_CORE c ON tmp.Claim_Number = c.Claim_Number
                            WHERE c.Claim_Number IS NOT NULL 
                            AND tmp.Claim_Number NOT in (select Claim_Number FROM travel.CLAIM_REJECT)
                        ) ; 
    
    
    /*Suppression des claims existants */
    DELETE FROM travel.CLAIM_CORE WHERE Id_Claim in 
                        (
                        SELECT  c.Id_Claim from travel.TMP_CLAIM_CORE tmp
                        LEFT OUTER JOIN travel.CLAIM_CORE c ON tmp.Claim_Number = c.Claim_Number
                        WHERE c.Claim_Number IS NOT NULL 
                        AND tmp.Claim_Number NOT in (select Claim_Number FROM travel.CLAIM_REJECT)
                        ) ;
    
    """


class InsertFinalTables:
    INSERT_FINAL_TABLES = """
        
        /*1- Insertion dans CLAIM_CORE TABLE*/
        INSERT INTO travel.claim_core
        (id_header, code_type_claimhandler, code_claimhandler, claim_number, subclaim_number, claim_description, code_business_line, dt_claim_open, dt_claim_reopen, dt_claim_close,
        dt_claim_declined, code_claim_status, code_claim_close_status, parent_claim_number, number_of_involved_people, dt_claim_reported, dt_claim_last_update, agent, occurrence_name,
        code_occurrence_type, code_trt_cause, dt_occurrence_begin, dt_occurrence_end, fg_catastrophic_event, code_catastrophe_type, catastrophe_name, code_industry_catastrophe, code_company_catastrophe,
        code_diagnosis, code_insurable_object_type, insurable_object_ea_ext_ref, booking, policy_number, dt_subscription, ref_policyholder, code_product, dt_effective, dt_expiration, 
        claim_type, occurrence_line_1_address, occurrence_line_2_address, occurrence_municipality_name, code_occurrence_state, code_occurrence_postal, code_occurence_country, occurrence_physical_location_name, occurrence_latitude_value, occurrence_longitude_value, occurrence_altitude_value, occurrence_altitude_sea_level_value, occurrence_horizontal_accuracy_value, occurrence_vertical_accuracy_value, occurrence_travel_direction, client, dt_insertion)
        select case when ch.Id_Header IS NULL OR ch.id_header = 0 THEN -1 ELSE ch.Id_Header end  id_header ,
        tcc.code_type_claimhandler,
        tcc.code_claimhandler,
        tcc.claim_number,
        tcc.subclaim_number,
        tcc.claim_description,
        tcc.code_business_line,
        tcc.dt_claim_open,
        tcc.dt_claim_reopen,
        tcc.dt_claim_close,
        tcc.dt_claim_declined,
        tcc.code_claim_status,
        tcc.code_claim_close_status,
        tcc.parent_claim_number,
        tcc.number_of_involved_people,
        tcc.dt_claim_reported,
        tcc.dt_claim_last_update,
        tcc.agent,
        tcc.occurrence_name,
        tcc.code_occurrence_type,
        tcc.code_trt_cause,
        tcc.dt_occurrence_begin,
        tcc.dt_occurrence_end,
        tcc.fg_catastrophic_event,
        tcc.code_catastrophe_type,
        tcc.catastrophe_name, 
        tcc.code_industry_catastrophe,
        tcc.code_company_catastrophe,
        tcc.code_diagnosis,
        tcc.code_insurable_object_type,
        tcc.insurable_object_ea_ext_ref,
        tcc.booking,
        policy_number,
        dt_subscription,
        tcc.ref_policyholder,
        tcc.code_product,
        tcc.dt_effective,
        tcc.dt_expiration,
        case 
        when substring(Claim_Number from 4 for 1)='T' then 'Technical' 
        when substring(Claim_Number from 4 for 1)='M' then 'Medical'
        else ''
        end claim_type,
        tcc.occurrence_line_1_address,
        tcc.occurrence_line_2_address, 
        tcc.occurrence_municipality_name,
        tcc.code_occurrence_state,
        tcc.code_occurrence_postal,
        tcc.code_occurence_country,
        tcc.occurrence_physical_location_name,
        tcc.occurrence_latitude_value,
        tcc.occurrence_longitude_value,
        tcc.occurrence_altitude_value,
        tcc.occurrence_altitude_sea_level_value,
        tcc.occurrence_horizontal_accuracy_value,
        tcc.occurrence_vertical_accuracy_value,
        tcc.occurrence_travel_direction,
        rp.client,
        tcc.dt_insertion
        from travel.tmp_claim_core tcc
        inner join travel.claim_header ch on tcc.routing_item =ch.routing_item and tcc.source_program =ch.source_program and tcc.dt_insertion =ch.dt_insertion 
        inner join travel.ref_product rp on tcc.code_product = rp.code_product 
        where tcc.Claim_Number not in (select Claim_Number FROM travel.claim_reject );
    
        /*2- Insertion dans CLAIM_PARTY TABLE*/
        INSERT INTO travel.claim_party
        (Id_Claim,claim_number,prefix_name,first_name,middle_name,last_name,suffix_name,full_legal_name,nickname, dt_birth,birth_place_name,code_birth_country,code_gender,line_1_address,
        line_2_address,municipality_name,code_state,code_postal,code_country,landline_phone_number,mobile_phone_number,personal_email_address,other_email_address,party_type,
        dt_claim_last_update,dt_insertion)
        SELECT 
        cc.id_claim Id_Claim,
        tcp.claim_number,
        tcp.prefix_name,
        tcp.first_name,
        tcp.middle_name,
        tcp.last_name,
        tcp.suffix_name,
        COALESCE(tcp.full_legal_name,tcp.last_name || ' ' || tcp.first_name) as full_legal_name,
        tcp.nickname, dt_birth,
        tcp.birth_place_name,
        tcp.code_birth_country,
        tcp.code_gender,
        tcp.line_1_address,
        tcp.line_2_address,
        tcp.municipality_name,
        tcp.code_state,
        tcp.code_postal,
        tcp.code_country,
        tcp.landline_phone_number,
        tcp.mobile_phone_number,
        tcp.personal_email_address,
        tcp.other_email_address,
        tcp.party_type,
        tcp.dt_claim_last_update,
        tcp.dt_insertion
        FROM travel.tmp_claim_party tcp 
        inner join travel.claim_header ch on tcp.routing_item =ch.routing_item and tcp.source_program =ch.source_program and tcp.dt_insertion =ch.dt_insertion 
        inner join travel.claim_core cc on  cc.id_header =ch.id_header and cc.claim_number =tcp.claim_number 
        where tcp.Claim_Number not in (select Claim_Number FROM travel.claim_reject )  ;
        
        /*3- Insertion dans CLAIM_COVERAGE_SERVICE TABLE*/
        insert into travel.CLAIM_COVERAGE_SERVICE
        (Id_Claim,Claim_Number,Id_Service_Order,Service_External_Reference,Code_Coverage,Code_Service_Type,Dt_Service_Creation,Code_Service_Status,Service_Supplier_Type,Service_Supplier_External_Reference
        ,Code_Service_Supplier_Org_Type,Service_Supplier_Org_Name,Service_Supplier_Alternate_Name,Service_Supplier_Acronym_Name,Code_Service_Supplier_Industry_Type,Code_Service_Supplier_Industry
        ,Service_Supplier_Dun_Bradstreet,Service_Supplier_Org_Description,Line_1_Address,Line_2_Address,Municipality_Name,Code_State,Code_Postal,Code_Country,Landline_Phone_Number
        ,Mobile_Phone_number,Personal_Email_Address,Other_Email_Address,Dt_Claim_Last_Update,Dt_Insertion)
        select  
        cc.Id_Claim
        ,ccs.Claim_Number
        ,1
        ,NULL
        ,99
        ,99
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,NULL
        ,ccs.Dt_Insertion
        from travel.tmp_claim_amount ccs
        inner join travel.claim_header ch on ccs.routing_item =ch.routing_item and ccs.source_program =ch.source_program and ccs.dt_insertion =ch.dt_insertion 
        inner join travel.claim_core cc on  cc.id_header =ch.id_header and cc.claim_number =ccs.claim_number 
        where ccs.Claim_Number not in (select Claim_Number FROM travel.claim_reject ) 
        and ccs.Amount_Level  in ('C','F')
        
        union all
        Select
        cc.Id_Claim
        ,ccs.Claim_Number
        ,ccs.Id_Service_Order
        ,ccs.Service_External_Reference
        ,ccs.Code_Coverage
        ,ccs.Code_Service_Type
        ,ccs.Dt_Service_Creation
        ,ccs.Code_Service_Status
        ,ccs.Service_Supplier_Type
        ,ccs.Service_Supplier_External_Reference
        ,ccs.Code_Service_Supplier_Org_Type
        ,ccs.Service_Supplier_Org_Name
        ,ccs.Service_Supplier_Alternate_Name
        ,ccs.Service_Supplier_Acronym_Name
        ,ccs.Code_Service_Supplier_Industry_Type
        ,ccs.Code_Service_Supplier_Industry
        ,ccs.Service_Supplier_Dun_Bradstreet
        ,ccs.Service_Supplier_Org_Description
        ,ccs.Line_1_Address
        ,ccs.Line_2_Address
        ,ccs.Municipality_Name
        ,ccs.Code_State
        ,ccs.Code_Postal
        ,ccs.Code_Country
        ,ccs.Landline_Phone_Number
        ,ccs.Mobile_Phone_number
        ,ccs.Personal_Email_Address
        ,ccs.Other_Email_Address
        ,ccs.Dt_Claim_Last_Update
        ,ccs.Dt_Insertion
        from travel.TMP_CLAIM_SERVICE ccs
        inner join travel.claim_header ch on ccs.routing_item =ch.routing_item and ccs.source_program =ch.source_program and ccs.dt_insertion =ch.dt_insertion 
        inner join travel.claim_core cc on  cc.id_header =ch.id_header and cc.claim_number =ccs.claim_number 
        where ccs.Claim_Number not in (select Claim_Number FROM travel.claim_reject )  ;
        
        /*4- Insertion dans CLAIM_AMOUNT TABLE*/
        insert into travel.CLAIM_AMOUNT
        (Id_Service,Id_Claim,Amount_Level,Claim_Number,Id_Service_Order,Service_Identifier_External_Ref,Id_Claim_Amount_Order,Claim_Amount_External_Ref,Invoice_External_Ref,Dt_Event,Code_Insurance_Type,Code_Amount_Type
        ,Debit_Credit,Dt_Value,Currency,Claim_Amount_Excl_Tax,Claim_Amount_Tax,Claim_Amount_Incl_Tax,Payee_Party_Type,Payee_External_Ref,Dt_Claim_Last_Update,Dt_Insertion)
        select 
        ccs.Id_Service
        ,cc.Id_Claim
        ,tca.Amount_Level
        ,tca.Claim_Number
        ,tca.Id_Service_Order
        ,tca.Service_Identifier_External_Ref
        ,tca.Id_Claim_Amount_Order
        ,tca.Claim_Amount_External_Ref
        ,tca.Invoice_External_Ref
        ,tca.Dt_Event
        ,tca.Code_Insurance_Type
        ,tca.Code_Amount_Type
        ,tca.Debit_Credit
        ,tca.Dt_Value
        ,tca.Currency
        ,tca.Claim_Amount_Excl_Tax
        ,tca.Claim_Amount_Tax
        ,tca.Claim_Amount_Incl_Tax
        ,tca.Payee_Party_Type
        ,tca.Payee_External_Ref
        ,tca.Dt_Claim_Last_Update
        ,tca.Dt_Insertion
        from travel.tmp_claim_amount tca
        inner join travel.claim_header ch on tca.routing_item =ch.routing_item and tca.source_program =ch.source_program and tca.dt_insertion =ch.dt_insertion 
        inner join travel.claim_core cc on  cc.id_header =ch.id_header and cc.claim_number =tca.claim_number 
        inner join travel.claim_coverage_service ccs on ccs.id_Claim = cc.id_Claim and  ccs.id_Service_Order = tca.id_Service_Order and ccs.service_external_reference =tca.service_Identifier_External_Ref
        where tca.Claim_Number not in (select Claim_Number FROM travel.claim_reject ) 
        and tca.Amount_Level  = 'S'  
        union all
        select 
        ccs.Id_Service
        ,cc.Id_Claim
        ,tca.Amount_Level
        ,tca.Claim_Number
        ,tca.Id_Service_Order
        ,tca.Service_Identifier_External_Ref
        ,tca.Id_Claim_Amount_Order
        ,tca.Claim_Amount_External_Ref
        ,tca.Invoice_External_Ref
        ,tca.Dt_Event
        ,tca.Code_Insurance_Type
        ,tca.Code_Amount_Type
        ,tca.Debit_Credit
        ,tca.Dt_Value
        ,tca.Currency
        ,tca.Claim_Amount_Excl_Tax
        ,tca.Claim_Amount_Tax
        ,tca.Claim_Amount_Incl_Tax
        ,tca.Payee_Party_Type
        ,tca.Payee_External_Ref
        ,tca.Dt_Claim_Last_Update
        ,tca.Dt_Insertion
        from travel.tmp_claim_amount tca
        inner join travel.claim_header ch on tca.routing_item =ch.routing_item and tca.source_program =ch.source_program and tca.dt_insertion =ch.dt_insertion 
        inner join travel.claim_core cc on  cc.id_header =ch.id_header and cc.claim_number =tca.claim_number 
        inner join travel.claim_coverage_service ccs on ccs.id_Claim = cc.id_Claim  and ccs.Code_Coverage=99 and ccs.Code_Service_Type=99
        where tca.Claim_Number not in (select Claim_Number FROM travel.claim_reject ) 
        and tca.Amount_Level  in ('C','F')  ;
    
    """


class QueriesFactClaims(object):

    def __init__(self, period_obj):
        self.period = period_obj

    def alim_fact_claims(self):
        fact_claims = f"""
            call travel.alim_fact_claims('{self.period}');              
                
        """
        return fact_claims
