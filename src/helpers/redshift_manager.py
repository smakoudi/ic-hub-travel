import psycopg2


class RedshiftManager:
    @staticmethod
    def execute_sql_query(query, conn, query_name):
        """
        Method to execute a sql query in Redshift
        :param query:
        :return:
        """
        cursor = conn.cursor()
        try:
            cursor.execute(query)
            rowcount = cursor.rowcount
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(f"error: unable to execute query {query_name}: {error}")
            conn.rollback()
            cursor.close()
            return 0
        print(f"info: successfully executed query {query_name}: {rowcount} affected rows")
        cursor.close()
        return rowcount
