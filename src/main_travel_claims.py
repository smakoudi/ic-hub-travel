import boto3
import json
import re
import time
import datetime
from pyspark.sql import SQLContext
from pyspark import SparkContext
from pyspark.sql import SparkSession
from helpers.travel_xml_helper import TravelHelper
from helpers.travel_reject_helper import RejectTables, FinalTables
from pyspark.sql.functions import col, lit

# Initialisation de la session spark :
spark = SparkSession.builder \
    .appName("LOAD FROM XML TO TMP TABLES") \
    .config('spark.sql.codegen.wholeStage', False) \
    .getOrCreate()

# Config parameters
IC_HUB_BUCKET = "ic-hub-travel-dev"
CLAIMS_DIRECTORY = "claims"

# TODO: move this paramaters to a global table
CLMH_PREFIX = {
    "GMMI": "claims_11_TRAVELCLAIM",
    "ea-es": "claims_1_TRAVELCLAIM",
    "ea-fr": "claims_10_TRAVELCLAIM",
    "ea-be": "claims_13_TRAVELCLAIM",
    "ea-de": "claims_9_TRAVELCLAIM",
    "CHAPKA": "claims_7_TRAVELCLAIM"
}

sc = SparkContext.getOrCreate()
sqlContext = SQLContext(sc)
s3 = boto3.resource("s3")
client = boto3.client('s3')
claims_bucket = s3.Bucket(IC_HUB_BUCKET)


# Connection parameters
redshift_conn_params = {
    "host": "idea-dev-redshift-dataplatform.cfkw6w4by08e.eu-central-1.redshift.amazonaws.com",
    "database": "ideadev",
    "user": "redshiftuser",
    "password": "Redshift2020",
    "port": 5439
}

# load the XSD file in the spark context variable
sc.addFile("s3://ic-hub-travel-param/CLMH-XSD/travelclaim27NodeVersion.xsd")

# load the mapping variables
mapping_xml_tmp_claim_core = json.loads(
    s3.Object("ic-hub-travel-param", "mapping-files/tmp-tables/mapping_xml_tmp_claim_core.json").get()['Body'].read())

mapping_xml_tmp_claim_service = json.loads(
    s3.Object("ic-hub-travel-param", "mapping-files/tmp-tables/mapping_xml_tmp_claim_service.json").get()[
        'Body'].read())
mapping_xml_tmp_claim_service_list_cols = [col.get('alias') for col in mapping_xml_tmp_claim_service if
                                           'CLAIM_COVERAGE_SERVICES' in col.get('full_path')]
mapping_xml_tmp_claim_service_flat_cols = [col.get('alias') for col in mapping_xml_tmp_claim_service if
                                           'CLAIM_COVERAGE_SERVICES' not in col.get('full_path')]

mapping_xml_tmp_claim_amount_service_1 = json.loads(s3.Object("ic-hub-travel-param", "mapping-files/tmp-tables/mapping_xml_tmp_claim_amount_service_1.json").get()['Body'].read())
mapping_xml_tmp_claim_amount_service_2 = json.loads(s3.Object("ic-hub-travel-param", "mapping-files/tmp-tables/mapping_xml_tmp_claim_amount_service_2.json").get()['Body'].read())

mapping_xml_tmp_claim_amount_global = json.loads(s3.Object("ic-hub-travel-param", "mapping-files/tmp-tables/mapping_xml_tmp_claim_amount_global.json").get()['Body'].read())
mapping_xml_tmp_claim_amount_first_reserve = json.loads(s3.Object("ic-hub-travel-param", "mapping-files/tmp-tables/mapping_xml_tmp_claim_amount_first_reserve.json").get()['Body'].read())
mapping_xml_tmp_claim_party_clm = json.loads(s3.Object("ic-hub-travel-param", "mapping-files/tmp-tables/mapping_xml_tmp_claim_party_clm.json").get()['Body'].read())
mapping_xml_tmp_claim_party_benef = json.loads(s3.Object("ic-hub-travel-param", "mapping-files/tmp-tables/mapping_xml_tmp_claim_party_benef.json").get()['Body'].read())

if __name__ == "__main__":

    start_time = time.time()
    print(f"*********    START TRAVEL CLAIMS SCRIPT AT {datetime.datetime.now()}     ************")

    conn = TravelHelper.connect_redshift(redshift_conn_params)

    for clmh in CLMH_PREFIX.items():
        print("-------------------------------------------------------")
        clmh_dir = f"{CLAIMS_DIRECTORY}/{clmh[0]}/"
        obj_in_directory = claims_bucket.objects.filter(Prefix=clmh_dir, Delimiter="/")
        if list(obj_in_directory):
            for obj in obj_in_directory:
                if TravelHelper.is_file(obj.key):
                    naming_match = re.search(f".*\/{clmh[1]}.*\.xml$", obj.key)
                    dt_insertion = datetime.datetime.now()
                    today = datetime.date.today()
                    first = today.replace(day=1)
                    lastMonth = first - datetime.timedelta(days=1)
                    period=lastMonth.strftime('%Y%m')
                    print(f"info: period {period}")
                    print(f"info: Recording date {dt_insertion} for {obj.key}")
                    if naming_match:
                        print(f"info: file {obj.key} match naming rule. next step: XSD check")
                        # check 1 succeded : go to check2
                        # read the XML file with the XSD validation
                        df_xsd = sqlContext.read.format('com.databricks.spark.xml').option("rootTag", "HEADER").option("rowValidationXSDPath", "travelclaim27NodeVersion.xsd").load(f"s3://{IC_HUB_BUCKET}/{obj.key}")
                        nb_corrupted_lines = df_xsd.count()
                        if nb_corrupted_lines == 0:
                            # check 2 succedded : go to mapping
                            print(f"info: file {obj.key} match XSD validation. next step: mapping")
                            # read the xml file with rowTag = CLAIM
                            deleted_rows = RejectTables.delete_staging_tables(conn)
                            print(f"info : deleted {deleted_rows} rows from staging tables")
                            xml_df = spark.read.format('xml').options(rowTag="CLAIM").load(f"s3://{IC_HUB_BUCKET}/{obj.key}")
                            xml_df_columns = TravelHelper.flatten_xml_schema(xml_df)
                            print(f"info: XML file {obj.key} contains {xml_df.count()} rows and {len(xml_df_columns)} columns")

                            dfHEADER = spark.read.format("xml").option("rowTag", "HEADER").load(f"s3://{IC_HUB_BUCKET}/{obj.key}")

                            Routing_Item = dfHEADER.select(col("Routing_Id").alias("Routing_Item")).first()[0]
                            Source_Program = dfHEADER.select(col("Routing_Source").alias("Source_Program")).first()[0]

                            # create the TMP_CLAIM_CORE
                            tmp_claim_core = TravelHelper.create_tmp_table(df=xml_df,
                                                                           mapping=mapping_xml_tmp_claim_core,
                                                                           df_columns=xml_df_columns,
                                                                           table_name="tmp_claim_core")
                            tmp_claim_core = tmp_claim_core.withColumn("Routing_Item", lit(Routing_Item)).withColumn("Source_Program", lit(Source_Program)).withColumn("dt_insertion", lit(dt_insertion))
                            tmp_claim_core.cache()
                            tmp_claim_core.printSchema()
                            TravelHelper.insert_into_redshift(conn, tmp_claim_core, "travel.tmp_claim_core")

                            #create the TMP_CLAIM_SERVICE
                            tmp_claim_service = TravelHelper.create_tmp_table(df=xml_df,
                                                                              mapping=mapping_xml_tmp_claim_service,
                                                                              df_columns=xml_df_columns,
                                                                              table_name="tmp_claim_service")
                            tmp_claim_service.cache()
                            flat_tmp_claim_service = TravelHelper.flatten_list_columns(tmp_claim_service,
                                                                                       mapping_xml_tmp_claim_service_flat_cols,
                                                                                       mapping_xml_tmp_claim_service_list_cols,
                                                                                       table_name="tmp_claim_service")
                            flat_tmp_claim_service = flat_tmp_claim_service.withColumn("Routing_Item", lit(Routing_Item)).withColumn("Source_Program", lit(Source_Program)).withColumn("dt_insertion", lit(dt_insertion))
                            TravelHelper.insert_into_redshift(conn, flat_tmp_claim_service, "travel.tmp_claim_service")

                            # create the TMP_CLAIM_AMOUNT_SERVICE
                            tmp_claim_amount_service = TravelHelper.create_tmp_table(df=xml_df,
                                                                                     mapping=mapping_xml_tmp_claim_amount_service_1,
                                                                                     df_columns=xml_df_columns,
                                                                                     table_name="tmp_claim_amount_service")
                            tmp_claim_amount_service.cache()
                            mapping_flat_cols = ['Claim_Number', 'Dt_Claim_Last_Update']
                            mapping_cols_to_flatten = ['Service_Identifier_External_Ref', 'CLAIM_COVERAGE_SERVICE_AMOUNTS', 'Id_Service_Order']
                            tmp_claim_amount_service_flat = TravelHelper.flatten_list_columns(df=tmp_claim_amount_service,
                                                                                              mapping_flat_cols=mapping_flat_cols,
                                                                                              mapping_cols_to_flatten=mapping_cols_to_flatten,
                                                                                              table_name='tmp_claim_amount_service_flat')
                            tmp_claim_amount_service_flat2 = TravelHelper.create_tmp_table(df=tmp_claim_amount_service_flat,
                                                                                           mapping=mapping_xml_tmp_claim_amount_service_2,
                                                                                           df_columns=TravelHelper.flatten_xml_schema(tmp_claim_amount_service_flat),
                                                                                           table_name='tmp_claim_amount_service_flat2')
                            tmp_claim_amount_service_flat2.cache()
                            mapping_flat_cols3 = ['Claim_Number', 'Service_Identifier_External_Ref', 'Dt_Claim_Last_Update', 'Id_Service_Order']
                            mapping_cols_to_flatten3 = [col for col in tmp_claim_amount_service_flat2.columns if col not in mapping_flat_cols3]
                            tmp_claim_amount_service_flat3 = TravelHelper.flatten_list_columns(
                                                                                    df=tmp_claim_amount_service_flat2,
                                                                                    mapping_flat_cols=mapping_flat_cols3,
                                                                                    mapping_cols_to_flatten=mapping_cols_to_flatten3,
                                                                                    table_name='tmp_claim_amount_service_flat3')
                            tmp_claim_amount_service_flat3.cache()
                            tmp_claim_amount_service_flat3 = tmp_claim_amount_service_flat3.withColumn("Routing_Item", lit(Routing_Item)).withColumn("Source_Program", lit(Source_Program)).withColumn("Amount_Level", lit("S")).withColumn("dt_insertion", lit(dt_insertion)).where(col("code_amount_type").isNotNull())
                            TravelHelper.insert_into_redshift(conn, tmp_claim_amount_service_flat3, "travel.tmp_claim_amount")

                            # create the TMP_CLAIM_AMOUNT_GLOBAL
                            tmp_claim_amount_global = TravelHelper.create_tmp_table(df=xml_df,
                                                                                    mapping=mapping_xml_tmp_claim_amount_global,
                                                                                    df_columns=xml_df_columns,
                                                                                    table_name="tmp_claim_amount_global")
                            tmp_claim_amount_global.cache()
                            tmp_claim_amount_global = tmp_claim_amount_global.withColumn("Routing_Item", lit(Routing_Item)).withColumn("Source_Program", lit(Source_Program)).withColumn("Amount_Level", lit("C")).withColumn("dt_insertion", lit(dt_insertion))
                            if 'Code_Amount_Type' in tmp_claim_amount_global.columns:
                                tmp_claim_amount_global = tmp_claim_amount_global.where(col("code_amount_type").isNotNull())
                                TravelHelper.insert_into_redshift(conn, tmp_claim_amount_global, "travel.tmp_claim_amount")
                            else:
                                print(f"warning: no Global Amount Tag in the file {obj.key}")

                            # create the TMP_CLAIM_AMOUNT_FIRST_RESERVE
                            tmp_claim_amount_first_reserve = TravelHelper.create_tmp_table(df=xml_df,
                                                                                           mapping=mapping_xml_tmp_claim_amount_first_reserve,
                                                                                           df_columns=xml_df_columns,
                                                                                           table_name="tmp_claim_amount_first_reserve")
                            tmp_claim_amount_first_reserve.cache()
                            if 'Code_Amount_Type' in tmp_claim_amount_first_reserve.columns:
                                tmp_claim_amount_first_reserve = tmp_claim_amount_first_reserve.withColumn("Routing_Item", lit(Routing_Item)).withColumn("Source_Program", lit(Source_Program)).withColumn("Amount_Level", lit("F")).withColumn("dt_insertion", lit(dt_insertion)).where(col("code_amount_type").isNotNull())
                                TravelHelper.insert_into_redshift(conn, tmp_claim_amount_first_reserve, "travel.tmp_claim_amount")
                            else:
                                print(f"warning: no First Reserve Tag in the file {obj.key}")

                            # create the TMP_CLAIM_PARTY_CLM
                            tmp_claim_party_clm = TravelHelper.create_tmp_table(df=xml_df,
                                                                                mapping=mapping_xml_tmp_claim_party_clm,
                                                                                df_columns=xml_df_columns,
                                                                                table_name="tmp_claim_party_clm")
                            tmp_claim_party_clm.cache()
                            tmp_claim_party_clm = tmp_claim_party_clm.withColumn("Routing_Item", lit(Routing_Item)).withColumn("Source_Program", lit(Source_Program)).withColumn("Party_Type",lit("Claimant")).withColumn("dt_insertion", lit(dt_insertion))
                            TravelHelper.insert_into_redshift(conn, tmp_claim_party_clm, "travel.tmp_claim_party")

                            # create the TMP_CLAIM_PARTY_BENEF
                            tmp_claim_party_benef = TravelHelper.create_tmp_table(df=xml_df,
                                                                                  mapping=mapping_xml_tmp_claim_party_benef,
                                                                                  df_columns=xml_df_columns,
                                                                                  table_name="tmp_claim_party_benef")
                            tmp_claim_party_benef.cache()
                            tmp_claim_party_benef_flat = TravelHelper.flatten_list_columns(
                                                                                    df=tmp_claim_party_benef,
                                                                                    mapping_flat_cols=['Claim_Number', 'Dt_Claim_Last_Update'],
                                                                                    mapping_cols_to_flatten=tmp_claim_party_benef.columns,
                                                                                    table_name='tmp_claim_party_benef_flat')
                            tmp_claim_party_benef_flat.cache()
                            tmp_claim_party_benef_flat = tmp_claim_party_benef_flat.withColumn("Routing_Item", lit(Routing_Item)).withColumn("Source_Program", lit(Source_Program)).withColumn("Party_Type", lit("Beneficiary")).withColumn("dt_insertion", lit(dt_insertion))
                            TravelHelper.insert_into_redshift(conn, tmp_claim_party_benef_flat, "travel.tmp_claim_party")

                            # delete duplicate rows from staging tables
                            duplicate_rows_staging = RejectTables.delete_duplicates(conn)
                            print(f"info : delete {duplicate_rows_staging} duplicate rows ")

                            # insert in reject tables
                            filename = obj.key.split('/')[-1]
                            reject_rgf = RejectTables.insert_reject_rgf(conn, filename)
                            reject_ref = RejectTables.insert_reject_ref(conn, filename)

                            # insert in header table
                            ClmHdr_Code = filename.replace("claims_", "")[0:filename.replace("claims_", "").index('_')]
                            headerColumns = dfHEADER.select(
                                  col("Extraction_Dt").alias("Dt_Functional_Extraction"),
                                  col("Routing_Id").alias("Routing_Item"),
                                  col("Routing_Dt").alias("Dt_Routing"),
                                  col("Routing_Source").alias("Source_Program"),
                                  col("Routing_Type").alias("Mode_Batch")).\
                                withColumn("file_name", lit(filename)).\
                                withColumn("Code_ClmHdr", lit(ClmHdr_Code)).\
                                withColumn("name_clmhdr", lit(clmh[0])).\
                                withColumn("nb_file ", lit(tmp_claim_core.count())).\
                                withColumn("nb_service", lit(flat_tmp_claim_service.count())).\
                                withColumn("nb_rejected_file ", lit(reject_rgf + reject_ref)).\
                                withColumn("dt_insertion", lit(dt_insertion))
                            TravelHelper.insert_into_redshift(conn, headerColumns, "travel.CLAIM_HEADER")

                            # insert into Hist tables
                            insert_hist_tables = FinalTables.insert_hist_tables(conn)
                            print(f"info : insert {insert_hist_tables} rows into hist tables")

                            # delete existing rows before insert into final table : UPSERT mode
                            delete_existing_rows = FinalTables.delete_exist_claims(conn)
                            print(f"info : Upsert mode; delete {delete_existing_rows} existing rows from final tables")

                            # insert into Final tables
                            insert_final_tables = FinalTables.insert_fianl_tables(conn)
                            print(f"info : insert {insert_final_tables} rows into final tables")

                            # insert into fact_table
                            alim_fact_claims = FinalTables.alim_fact_claims(conn, period)
                            print(f"info : insert into fact_claims table done")

                            # insert into reject logs tables
                            reject_logs = RejectTables.insert_reject_logs(conn, clmh[0])
                        else:
                            # check 2 failed: move file to rejected directory
                            TravelHelper.move_rejected_file(clmh, obj)
                    else:
                        print(f"error: file {obj.key} does not match naming rule")
                        # check 1 failed : move file to rejected directory
                        TravelHelper.move_rejected_file(clmh, obj)
                else:
                    print(f"warning: file {obj.key} is a directory")

        else:
            print(f"error: s3 directory {clmh_dir} not found or empty ")

    full_execution_time = time.time() - start_time
    print(f"************      THE TRAVEL CLAIMS SCRIPT TOOK {full_execution_time} SECONDS     ********************")
