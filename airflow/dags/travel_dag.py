import datetime

from airflow.contrib.operators.emr_create_job_flow_operator import EmrCreateJobFlowOperator
from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.contrib.operators.emr_add_steps_operator import EmrAddStepsOperator
from airflow.contrib.sensors.emr_step_sensor import EmrStepSensor
from airflow.providers.amazon.aws.hooks.base_aws import AwsBaseHook
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.postgres_hook import PostgresHook

#TODO: configure the IAM  permissions for the environment assumed role
# The policies for the DAG to work :
# elasticmapreduce:RunJobFlow => added as MWAA-ElasticMapReduce-allow-all
# iam:PassRole on the EMR_DefaultRole & EMR_EC2_DefaultRole => added as MWAA-PassRole-EMR_DefaultRole


default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "email": ["kzinebi@externe.europ-assistance.com"],
    "email_on_failure": False,
    "email_on_retry": False
}



JOB_FLOW_OVERRIDES = {
    "Name": "ic-hub-travel-airflow",
    "ReleaseLabel": "emr-6.4.0",
    "LogUri": "s3://ic-hub-travel-param/DAGs/airflow_logs/",
    "Applications": [
                        {"Name": "Hadoop"},
                        {"Name": "Spark"},
                        {"Name": "Hive"},
                    ],
    "Tags": [{"Key": "Environnement", "Value": "DEV/QA"}, {"Key": "Projet", "Value": "ic-hub"}],
    "Instances": {
        "InstanceGroups": [
            {
                "Name": "MASTER",
                "Market": "ON_DEMAND",
                "InstanceRole": "MASTER",
                "InstanceType": "m5.xlarge",
                "InstanceCount": 1,
            },
            {
                "Name": "CORE- 2",
                "Market": "ON_DEMAND",
                "InstanceRole": "CORE",
                "InstanceType": "m5.xlarge",
                "InstanceCount": 2,
            },
        ],
        "KeepJobFlowAliveWhenNoSteps": False,
        "TerminationProtected": False,
        "Ec2KeyName": "EMR_KEY",
        "Ec2SubnetId": "subnet-016426c6101dfe170"
    },
    "JobFlowRole": "EMR_EC2_DefaultRole",
    "ServiceRole": "EMR_DefaultRole",
}

SPARK_STEPS = [
        {
            "Name": "Copy scripts to cluster",
            "ActionOnFailure": "TERMINATE_CLUSTER",
            "HadoopJarStep": {
                "Jar": "command-runner.jar",
                "Args": [
                    "aws", "s3", "cp", "s3://ic-hub-travel-param/ic-hub-travel-scripts/", "/home/hadoop/", "--recursive"
                ]
            }
        },
        {
            "Name": "Copy jar files to cluster",
            "ActionOnFailure": "TERMINATE_CLUSTER",
            "HadoopJarStep": {
                "Jar": "command-runner.jar",
                "Args": [
                    "sudo", "aws", "s3", "cp", "s3://ic-hub-travel-param/jars/", "/home/hadoop/", "--recursive"
                ]
            }
        },
        {
            "Name": "Install requirements",
            "ActionOnFailure": "CONTINUE",
            "HadoopJarStep": {
                "Jar": "command-runner.jar",
                "Args": [
                    "pip3", "install", "boto3==1.20.26", "pandas==1.3.5", "numpy==1.21.5", "psycopg2-binary==2.9.2"
                ]
            }
        },
        {
            "Name": "Execute runner",
            "ActionOnFailure": "CONTINUE",
            "HadoopJarStep": {
                "Jar": "command-runner.jar",
                "Args": [
                    "spark-submit", "--jars", "/home/hadoop/spark-xml_2.12-0.10.0.jar", "/home/hadoop/src/main_travel_claims.py"
                ]
            }
        }
    ]

aws_hook = AwsBaseHook(aws_conn_id="aws_default", client_type="emr")
emr_client = aws_hook.get_client_type('emr')


def insert_log_task_stat(**context):
    redshift_hook = PostgresHook(postgres_conn_id='redshift', schema='ichubdev')
    conn = redshift_hook.get_conn()
    cursor = conn.cursor()

    dr = context['dag_run']
    all_emr_clusters = emr_client.list_clusters(CreatedAfter=dr.execution_date)
    travel_cluster = [c for c in all_emr_clusters.get('Clusters') if c.get('Name') == JOB_FLOW_OVERRIDES['Name']][0]['Id']
    describe_cluster = emr_client.describe_cluster(ClusterId=travel_cluster)
    print(f"describe_cluster : {describe_cluster}")
    emr_start_dt = describe_cluster.get('Cluster', {}).get('Status', {}).get('Timeline', {}).get('CreationDateTime')
    emr_ready_dt = describe_cluster.get('Cluster', {}).get('Status', {}).get('Timeline', {}).get('ReadyDateTime')
    if emr_start_dt and emr_ready_dt:
        emr_creation_duration = (emr_ready_dt - emr_start_dt).seconds
        query_emr_creation = f"""
                                insert into ichubdev.ic_hub_logs.task_stat values
                                ('ic-hub-travel', 'create_emr_cluster', '{emr_start_dt}', '{emr_ready_dt}', '{emr_creation_duration}', 'COMPLETED', null, '{datetime.datetime.now()}')
                              """
        print(f"query : {query_emr_creation}")
        try:
            cursor.execute(query_emr_creation)
            conn.commit()
        except Exception as e:
            print(f"ERROR: unable to insert logs in task_log table : {e}")

    describe_steps = emr_client.list_steps(ClusterId=travel_cluster)
    if describe_steps.get('Steps'):
        for step in describe_steps.get('Steps'):
            id_task = step.get('Id')
            status = step.get('Status', {}).get('State')
            dt_start = step.get('Status').get('Timeline').get('StartDateTime')
            print(f"dt_start : {dt_start}")
            dt_end = step.get('Status').get('Timeline').get('EndDateTime')
            print(f"dt_end : {dt_end}")
            try:
                duration = (dt_end - dt_start).seconds
            except Exception as e:
                print(f"Error: {e}")
                duration = None
            print(f"duration : {duration}")
            error_msg = step.get('Status', {}).get('FailureDetails', {}).get('Reason')
            if dt_start and dt_end and duration:
                query = f"""
                        insert into ichubdev.ic_hub_logs.task_stat values
                        ('ic-hub-travel', '{id_task}', '{dt_start}', '{dt_end}', '{duration}', '{status}', '{error_msg}', '{datetime.datetime.now()}')
                      """
            else:
                dt_start = 'null'
                dt_end = 'null'
                duration = 'null'
                query = f"""
                        insert into ichubdev.ic_hub_logs.task_stat values
                        ('ic-hub-travel', '{id_task}', {dt_start}, {dt_end}, {duration}, '{status}', '{error_msg}', '{datetime.datetime.now()}')
                      """
            print(f"query : {query}")
            try:
                cursor.execute(query)
                conn.commit()
            except Exception as e:
                print(f"ERROR: unable to insert logs in task_log table : {e}")


with DAG(
    dag_id='ic-hub-travel',
    default_args=default_args,
    start_date=days_ago(1),
    schedule_interval=None,  #TODO: Add the cron expression to run the dag
    tags=['ic-hub', 'travel', 'emr'],
) as dag:

    create_emr_cluster = EmrCreateJobFlowOperator(
                                            task_id="create_emr_cluster",
                                            job_flow_overrides=JOB_FLOW_OVERRIDES,
                                            aws_conn_id="aws_default",
                                            emr_conn_id="emr_default",
                                            dag=dag
                                        )

    execute_runner = EmrAddStepsOperator(
                            task_id="execute_runner",
                            job_flow_id="{{ task_instance.xcom_pull(task_ids='create_emr_cluster', key='return_value') }}",
                            aws_conn_id="aws_default",
                            steps=SPARK_STEPS,
                            dag=dag
                        )

    check_execute_runner = EmrStepSensor(
                            task_id='check_execute_runner',
                            job_flow_id="{{ task_instance.xcom_pull(task_ids='create_emr_cluster', key='return_value') }}",
                            step_id="{{ task_instance.xcom_pull('execute_runner', key='return_value')[0] }}",
                            aws_conn_id='aws_default',
                            dag=dag
                        )

    insert_log_task_stat = PythonOperator(
                            dag=dag,
                            task_id="insert_log_task_stat",
                            python_callable=insert_log_task_stat,
                            op_kwargs=None,
                            provide_context=True
                        )


create_emr_cluster >> execute_runner >> check_execute_runner >> insert_log_task_stat